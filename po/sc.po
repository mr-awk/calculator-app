# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-02-08 16:37+0100\n"
"PO-Revision-Date: 2018-07-29 00:00+0000\n"
"Last-Translator: Adrià <adriamartinmor@gmail.com>\n"
"Language-Team: Sardinian <https://translate.ubports.com/projects/ubports/"
"calculator-app/sc/>\n"
"Language: sc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.19.1\n"

#: ../app/engine/formula.js:176
msgid "NaN"
msgstr "NaN"

#: ../app/ubuntu-calculator-app.qml:267
#: /tmp/tmp.WzlPraBXv6/po/ubuntu-calculator-app.desktop.in.in.h:1
msgid "Calculator"
msgstr "Carculadora"

#: ../app/ubuntu-calculator-app.qml:271 ../app/ubuntu-calculator-app.qml:276
msgid "Favorite"
msgstr "Preferidu"

#: ../app/ubuntu-calculator-app.qml:323
msgid "Cancel"
msgstr "Annulla"

#: ../app/ubuntu-calculator-app.qml:333
msgid "Select All"
msgstr "Seletziona totus"

#: ../app/ubuntu-calculator-app.qml:333
msgid "Select None"
msgstr "Non seletziones nudda"

#: ../app/ubuntu-calculator-app.qml:340 ../app/ubuntu-calculator-app.qml:404
msgid "Copy"
msgstr "Còpia"

#: ../app/ubuntu-calculator-app.qml:348 ../app/ubuntu-calculator-app.qml:450
msgid "Delete"
msgstr "Cantzella"

#: ../app/ubuntu-calculator-app.qml:414
msgid "Edit"
msgstr "Modìfica"

#: ../app/ubuntu-calculator-app.qml:428
msgid "Add to favorites"
msgstr "Agiunghe a is preferidos"

#. TRANSLATORS: this is a time formatting string, see
#. http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for
#. valid expressions
#. TRANSLATORS: this is a time formatting string, see
#. http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid
#. expressions
#: ../app/ubuntu-calculator-app.qml:566 ../app/ui/FavouritePage.qml:89
#: ../app/ui/Screen.qml:51
msgid "dd MMM yyyy"
msgstr "dd MMM yyyy"

#: ../app/ui/FavouritePage.qml:42
msgid "No favorites"
msgstr "Non bi sunt preferidos"

#: ../app/ui/FavouritePage.qml:43
msgid ""
"Swipe calculations to the left\n"
"to mark as favorites"
msgstr ""
"Iscurre is càrculos a manca\n"
"pro los agiùnghere a is preferidos"

#. TRANSLATORS Natural logarithm symbol (logarithm to the base e)
#: ../app/ui/LandscapeKeyboard.qml:37 ../app/ui/PortraitKeyboard.qml:60
msgid "log"
msgstr "log"

#. TRANSLATORS Modulo operation: Finds the remainder after division of one number by another
#: ../app/ui/LandscapeKeyboard.qml:44 ../app/ui/PortraitKeyboard.qml:64
msgid "mod"
msgstr "mod"

#: ../app/ui/Screen.qml:37
msgid "Just now"
msgstr "Immoe etotu"

#: ../app/ui/Screen.qml:39
msgid "Today "
msgstr "Oe "

#. TRANSLATORS: this is a time formatting string, see
#. http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for
#. valid expressions
#: ../app/ui/Screen.qml:43
msgid "hh:mm"
msgstr "hh:mm"

#: ../app/ui/Screen.qml:45
msgid "Yesterday"
msgstr "Eris"

#: ../app/ui/Walkthrough.qml:96
msgid "Skip"
msgstr "Brinca"

#: ../app/welcomewizard/Slide1.qml:27
msgid "Welcome to Calculator"
msgstr "Ti donamus su benebènnidu a sa Carculadora"

#: ../app/welcomewizard/Slide1.qml:28
msgid "Enjoy the power of math by using Calculator"
msgstr "Ispàssia·ti cun sa potèntzia de sa matemàtica impreende sa Carculadora"

#: ../app/welcomewizard/Slide10.qml:27
msgid "Copy formula"
msgstr "Còpia sa fòrmula"

#: ../app/welcomewizard/Slide10.qml:28
msgid "Long press to copy part or all of a formula to the clipboard"
msgstr ""
"Mantene sa pressione pro copiare una parte o totu sa fòrmula a sa punta de "
"billete"

#: ../app/welcomewizard/Slide11.qml:79
msgid "Enjoy"
msgstr "Ispàssia·ti"

#: ../app/welcomewizard/Slide11.qml:88
msgid "We hope you enjoy using Calculator!"
msgstr "Isperamus chi t'ispàssies impreende sa Calculadora!"

#: ../app/welcomewizard/Slide11.qml:106
msgid "Finish"
msgstr "Acabba"

#: ../app/welcomewizard/Slide2.qml:27
msgid "Scientific keyboard"
msgstr "Tecladu iscientìficu"

#: ../app/welcomewizard/Slide2.qml:28
msgid "Access scientific functions with a left swipe on the numeric keypad"
msgstr ""
"Atzede a is funtziones iscientìficas cun un'iscurrimentu a manca in su "
"tecladu numèricu"

#: ../app/welcomewizard/Slide3.qml:27
msgid "Scientific View"
msgstr "Modalidade iscientìfica"

#: ../app/welcomewizard/Slide3.qml:28
msgid "Rotate device to show numeric and scientific functions together"
msgstr ""
"Gira su dispositivu pro ammustrare is funtziones numèricas e iscientìficas "
"in paris"

#: ../app/welcomewizard/Slide4.qml:27
msgid "Delete item from calculation history"
msgstr "Cantzella un'elementu dae sa cronologia de is càrculos"

#: ../app/welcomewizard/Slide4.qml:28
msgid "Swipe right to delete items from calculator history"
msgstr ""
"Iscurre su didu a dereta pro cantzellare is elementos dae sa cronologia de "
"is càrculos"

#: ../app/welcomewizard/Slide5.qml:27
msgid "Delete several items from calculation history"
msgstr "Cantzella vàrios elementos dae sa cronologia de is càrculos"

#: ../app/welcomewizard/Slide5.qml:28
msgid "Long press to select multiple calculations for deletion"
msgstr "Mantene sa pressione pro eliminare vàrios càrculos"

#: ../app/welcomewizard/Slide6.qml:27
msgid "Delete whole formula at once"
msgstr "Cantzella sa fòrmula intrea"

#: ../app/welcomewizard/Slide6.qml:28
msgid "Long press '←' button to clear all formulas from input bar"
msgstr ""
"Mantene sa pressione de su pulsante '←' pro limpiare totus is fòrmulas dae "
"sa barra de intrada"

#: ../app/welcomewizard/Slide7.qml:27
msgid "Edit item from calculation history"
msgstr "Modìfica s'elementu dae sa cronologia de is càrculos"

#: ../app/welcomewizard/Slide7.qml:28
msgid "Swipe left and press pencil to edit calculation"
msgstr "Iscurre a manca e carca su lapis pro modificare is càrculos"

#: ../app/welcomewizard/Slide8.qml:27
msgid "Add new favourite"
msgstr "Agiunghe preferidu"

#: ../app/welcomewizard/Slide8.qml:28
msgid "Swipe left and press star to add calculations to favourites view"
msgstr ""
"Iscurre a manca e carca s'istella pro agiùnghere is càrculos a is preferidos"

#: ../app/welcomewizard/Slide9.qml:27
msgid "Edit formula"
msgstr "Modìfica fòrmula"

#: ../app/welcomewizard/Slide9.qml:28
msgid "Click in the middle of a formula to edit in place"
msgstr "Incarca in mesu de una fòrmula pro la modificare"

#: /tmp/tmp.WzlPraBXv6/po/ubuntu-calculator-app.desktop.in.in.h:2
msgid "A calculator for Ubuntu."
msgstr "Una carculadora pro Ubuntu."

#: /tmp/tmp.WzlPraBXv6/po/ubuntu-calculator-app.desktop.in.in.h:3
msgid "math;addition;subtraction;multiplication;division;"
msgstr "additzione;sutratzione;carculadora;multiplicatzione;divisione;"
