# Afrikaans translation for ubuntu-calculator-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-calculator-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calculator-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-02-08 16:37+0100\n"
"PO-Revision-Date: 2017-08-18 09:11+0000\n"
"Last-Translator: Jean Joubert <developer-jsj@ourbillion.net>\n"
"Language-Team: Afrikaans <https://ubpweblate.tnvcomp.com/projects/ubports/"
"calculator-app/af/>\n"
"Language: af\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.15\n"
"X-Launchpad-Export-Date: 2017-04-05 07:42+0000\n"

#: ../app/engine/formula.js:176
#, fuzzy
msgid "NaN"
msgstr "NaN"

#: ../app/ubuntu-calculator-app.qml:267
#: /tmp/tmp.WzlPraBXv6/po/ubuntu-calculator-app.desktop.in.in.h:1
msgid "Calculator"
msgstr "Sakrekenaar"

#: ../app/ubuntu-calculator-app.qml:271 ../app/ubuntu-calculator-app.qml:276
msgid "Favorite"
msgstr "Gunsteling"

#: ../app/ubuntu-calculator-app.qml:323
msgid "Cancel"
msgstr "Kanselleer"

#: ../app/ubuntu-calculator-app.qml:333
msgid "Select All"
msgstr "Selekteer Alles"

#: ../app/ubuntu-calculator-app.qml:333
msgid "Select None"
msgstr "Selekteer Geen"

#: ../app/ubuntu-calculator-app.qml:340 ../app/ubuntu-calculator-app.qml:404
msgid "Copy"
msgstr "Kopieër"

#: ../app/ubuntu-calculator-app.qml:348 ../app/ubuntu-calculator-app.qml:450
msgid "Delete"
msgstr "Verwyder"

#: ../app/ubuntu-calculator-app.qml:414
msgid "Edit"
msgstr "Wysig"

#: ../app/ubuntu-calculator-app.qml:428
msgid "Add to favorites"
msgstr "Voeg by gunsteling"

#. TRANSLATORS: this is a time formatting string, see
#. http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for
#. valid expressions
#. TRANSLATORS: this is a time formatting string, see
#. http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid
#. expressions
#: ../app/ubuntu-calculator-app.qml:566 ../app/ui/FavouritePage.qml:89
#: ../app/ui/Screen.qml:51
#, fuzzy
msgid "dd MMM yyyy"
msgstr "dd MMM yyyy"

#: ../app/ui/FavouritePage.qml:42
msgid "No favorites"
msgstr "Geen gunsteling"

#: ../app/ui/FavouritePage.qml:43
msgid ""
"Swipe calculations to the left\n"
"to mark as favorites"
msgstr ""
"Swiep berekeninge na die linkerkant toe om dit as 'n gunsteling the merk"

#. TRANSLATORS Natural logarithm symbol (logarithm to the base e)
#: ../app/ui/LandscapeKeyboard.qml:37 ../app/ui/PortraitKeyboard.qml:60
#, fuzzy
msgid "log"
msgstr "log"

#. TRANSLATORS Modulo operation: Finds the remainder after division of one number by another
#: ../app/ui/LandscapeKeyboard.qml:44 ../app/ui/PortraitKeyboard.qml:64
#, fuzzy
msgid "mod"
msgstr "mod"

#: ../app/ui/Screen.qml:37
msgid "Just now"
msgstr "Net nou"

#: ../app/ui/Screen.qml:39
msgid "Today "
msgstr "Vandag "

#. TRANSLATORS: this is a time formatting string, see
#. http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for
#. valid expressions
#: ../app/ui/Screen.qml:43
#, fuzzy
msgid "hh:mm"
msgstr "hh:mm"

#: ../app/ui/Screen.qml:45
msgid "Yesterday"
msgstr "Gister"

#: ../app/ui/Walkthrough.qml:96
#, fuzzy
msgid "Skip"
msgstr "Ignoreer"

#: ../app/welcomewizard/Slide1.qml:27
msgid "Welcome to Calculator"
msgstr "Welkom na die Sakrekenaar"

#: ../app/welcomewizard/Slide1.qml:28
msgid "Enjoy the power of math by using Calculator"
msgstr "Geniet die mag van wiskunde met die Sakrekenaar"

#: ../app/welcomewizard/Slide10.qml:27
msgid "Copy formula"
msgstr "Kopieër formule"

#: ../app/welcomewizard/Slide10.qml:28
msgid "Long press to copy part or all of a formula to the clipboard"
msgstr ""
"Lang druk om 'n gedeelte van of die volledige formule na die knipbord te "
"kopieër"

#: ../app/welcomewizard/Slide11.qml:79
msgid "Enjoy"
msgstr "Geniet"

#: ../app/welcomewizard/Slide11.qml:88
msgid "We hope you enjoy using Calculator!"
msgstr "Ons hoop u geniet die Sakrekenaar!"

#: ../app/welcomewizard/Slide11.qml:106
msgid "Finish"
msgstr "Voltooi"

#: ../app/welcomewizard/Slide2.qml:27
msgid "Scientific keyboard"
msgstr "Wetenskaplike sleutelbord"

#: ../app/welcomewizard/Slide2.qml:28
#, fuzzy
msgid "Access scientific functions with a left swipe on the numeric keypad"
msgstr ""
"Verkry toegang na wetenskaplike funksies deur 'n linker swiep op die "
"numeriese sleutelbord"

#: ../app/welcomewizard/Slide3.qml:27
msgid "Scientific View"
msgstr "Wetenskaplike Uitleg"

#: ../app/welcomewizard/Slide3.qml:28
msgid "Rotate device to show numeric and scientific functions together"
msgstr "Draai toestel om numeriese en wetenskaplike funksies saam te wys"

#: ../app/welcomewizard/Slide4.qml:27
msgid "Delete item from calculation history"
msgstr "Verwyder item van die berekenings geskiedenis"

#: ../app/welcomewizard/Slide4.qml:28
msgid "Swipe right to delete items from calculator history"
msgstr "Swiep regs om die items van die berekenings geskiedenis te verwyder"

#: ../app/welcomewizard/Slide5.qml:27
msgid "Delete several items from calculation history"
msgstr "Verwyder verskeie items van die berekenings geskiedenis"

#: ../app/welcomewizard/Slide5.qml:28
msgid "Long press to select multiple calculations for deletion"
msgstr "Lang druk om verskeie berekeninge te verwyder"

#: ../app/welcomewizard/Slide6.qml:27
msgid "Delete whole formula at once"
msgstr "Verwyder die volledige formule onmiddellik"

#: ../app/welcomewizard/Slide6.qml:28
msgid "Long press '←' button to clear all formulas from input bar"
msgstr ""
"Lang druk die '←' knoppie om all die formules van die invoerbalk te verwyder"

#: ../app/welcomewizard/Slide7.qml:27
msgid "Edit item from calculation history"
msgstr "Wysig item van die berekenings geskiedenis"

#: ../app/welcomewizard/Slide7.qml:28
msgid "Swipe left and press pencil to edit calculation"
msgstr ""
"Swiep na die linkerkant en druk die potlood om die berekening the wysig"

#: ../app/welcomewizard/Slide8.qml:27
msgid "Add new favourite"
msgstr "Voeg nuwe gunsteling by"

#: ../app/welcomewizard/Slide8.qml:28
#, fuzzy
msgid "Swipe left and press star to add calculations to favourites view"
msgstr ""
"Swiep na die linkerkant toe en druk op die ster om die berekening by die "
"gunsteling uitleg te voeg"

#: ../app/welcomewizard/Slide9.qml:27
msgid "Edit formula"
msgstr "Wysig formule"

#: ../app/welcomewizard/Slide9.qml:28
#, fuzzy
msgid "Click in the middle of a formula to edit in place"
msgstr "Kliek in die middel van die formule om dit daar te wysig"

#: /tmp/tmp.WzlPraBXv6/po/ubuntu-calculator-app.desktop.in.in.h:2
msgid "A calculator for Ubuntu."
msgstr "'n Sakrekenaar vir Ubuntu."

#: /tmp/tmp.WzlPraBXv6/po/ubuntu-calculator-app.desktop.in.in.h:3
msgid "math;addition;subtraction;multiplication;division;"
msgstr "wiskunde;toevoeging;aftrekking;vermeenigvuldiging;afdeling;"
